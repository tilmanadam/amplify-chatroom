// tslint:disable
// this is an auto generated file. This will be overwritten

export const onCreateMessage = /* GraphQL */ `
  subscription OnCreateMessage {
    onCreateMessage {
      id
      inhalt
      cognitoId
      absender
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateMessage = /* GraphQL */ `
  subscription OnUpdateMessage {
    onUpdateMessage {
      id
      inhalt
      cognitoId
      absender
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteMessage = /* GraphQL */ `
  subscription OnDeleteMessage {
    onDeleteMessage {
      id
      inhalt
      cognitoId
      absender
      createdAt
      updatedAt
    }
  }
`;
export const onCreateMessagePublic = /* GraphQL */ `
  subscription OnCreateMessagePublic {
    onCreateMessagePublic {
      id
      inhalt
      absender
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateMessagePublic = /* GraphQL */ `
  subscription OnUpdateMessagePublic {
    onUpdateMessagePublic {
      id
      inhalt
      absender
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteMessagePublic = /* GraphQL */ `
  subscription OnDeleteMessagePublic {
    onDeleteMessagePublic {
      id
      inhalt
      absender
      createdAt
      updatedAt
    }
  }
`;

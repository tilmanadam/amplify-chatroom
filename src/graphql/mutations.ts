// tslint:disable
// this is an auto generated file. This will be overwritten

export const createMessage = /* GraphQL */ `
  mutation CreateMessage(
    $input: CreateMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    createMessage(input: $input, condition: $condition) {
      id
      inhalt
      cognitoId
      absender
      createdAt
      updatedAt
    }
  }
`;
export const updateMessage = /* GraphQL */ `
  mutation UpdateMessage(
    $input: UpdateMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    updateMessage(input: $input, condition: $condition) {
      id
      inhalt
      cognitoId
      absender
      createdAt
      updatedAt
    }
  }
`;
export const deleteMessage = /* GraphQL */ `
  mutation DeleteMessage(
    $input: DeleteMessageInput!
    $condition: ModelMessageConditionInput
  ) {
    deleteMessage(input: $input, condition: $condition) {
      id
      inhalt
      cognitoId
      absender
      createdAt
      updatedAt
    }
  }
`;
export const createMessagePublic = /* GraphQL */ `
  mutation CreateMessagePublic(
    $input: CreateMessagePublicInput!
    $condition: ModelMessagePublicConditionInput
  ) {
    createMessagePublic(input: $input, condition: $condition) {
      id
      inhalt
      absender
      createdAt
      updatedAt
    }
  }
`;
export const updateMessagePublic = /* GraphQL */ `
  mutation UpdateMessagePublic(
    $input: UpdateMessagePublicInput!
    $condition: ModelMessagePublicConditionInput
  ) {
    updateMessagePublic(input: $input, condition: $condition) {
      id
      inhalt
      absender
      createdAt
      updatedAt
    }
  }
`;
export const deleteMessagePublic = /* GraphQL */ `
  mutation DeleteMessagePublic(
    $input: DeleteMessagePublicInput!
    $condition: ModelMessagePublicConditionInput
  ) {
    deleteMessagePublic(input: $input, condition: $condition) {
      id
      inhalt
      absender
      createdAt
      updatedAt
    }
  }
`;

// tslint:disable
// this is an auto generated file. This will be overwritten

export const getMessage = /* GraphQL */ `
  query GetMessage($id: ID!) {
    getMessage(id: $id) {
      id
      inhalt
      cognitoId
      absender
      createdAt
      updatedAt
    }
  }
`;
export const listMessages = /* GraphQL */ `
  query ListMessages(
    $filter: ModelMessageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listMessages(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        inhalt
        cognitoId
        absender
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getMessagePublic = /* GraphQL */ `
  query GetMessagePublic($id: ID!) {
    getMessagePublic(id: $id) {
      id
      inhalt
      absender
      createdAt
      updatedAt
    }
  }
`;
export const listMessagePublics = /* GraphQL */ `
  query ListMessagePublics(
    $filter: ModelMessagePublicFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listMessagePublics(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        inhalt
        absender
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;

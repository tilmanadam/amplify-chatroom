/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type CreateMessageInput = {
  id?: string | null,
  inhalt?: string | null,
  cognitoId?: string | null,
  absender?: string | null,
  createdAt?: string | null,
  updatedAt?: string | null,
};

export type ModelMessageConditionInput = {
  inhalt?: ModelStringInput | null,
  cognitoId?: ModelStringInput | null,
  absender?: ModelStringInput | null,
  createdAt?: ModelStringInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelMessageConditionInput | null > | null,
  or?: Array< ModelMessageConditionInput | null > | null,
  not?: ModelMessageConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type UpdateMessageInput = {
  id: string,
  inhalt?: string | null,
  cognitoId?: string | null,
  absender?: string | null,
  createdAt?: string | null,
  updatedAt?: string | null,
};

export type DeleteMessageInput = {
  id?: string | null,
};

export type CreateMessagePublicInput = {
  id?: string | null,
  inhalt?: string | null,
  absender?: string | null,
  createdAt?: string | null,
  updatedAt?: string | null,
};

export type ModelMessagePublicConditionInput = {
  inhalt?: ModelStringInput | null,
  absender?: ModelStringInput | null,
  createdAt?: ModelStringInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelMessagePublicConditionInput | null > | null,
  or?: Array< ModelMessagePublicConditionInput | null > | null,
  not?: ModelMessagePublicConditionInput | null,
};

export type UpdateMessagePublicInput = {
  id: string,
  inhalt?: string | null,
  absender?: string | null,
  createdAt?: string | null,
  updatedAt?: string | null,
};

export type DeleteMessagePublicInput = {
  id?: string | null,
};

export type ModelMessageFilterInput = {
  id?: ModelIDInput | null,
  inhalt?: ModelStringInput | null,
  cognitoId?: ModelStringInput | null,
  absender?: ModelStringInput | null,
  createdAt?: ModelStringInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelMessageFilterInput | null > | null,
  or?: Array< ModelMessageFilterInput | null > | null,
  not?: ModelMessageFilterInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type ModelMessagePublicFilterInput = {
  id?: ModelIDInput | null,
  inhalt?: ModelStringInput | null,
  absender?: ModelStringInput | null,
  createdAt?: ModelStringInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelMessagePublicFilterInput | null > | null,
  or?: Array< ModelMessagePublicFilterInput | null > | null,
  not?: ModelMessagePublicFilterInput | null,
};

export type CreateMessageMutationVariables = {
  input: CreateMessageInput,
  condition?: ModelMessageConditionInput | null,
};

export type CreateMessageMutation = {
  createMessage:  {
    __typename: "Message",
    id: string,
    inhalt: string | null,
    cognitoId: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type UpdateMessageMutationVariables = {
  input: UpdateMessageInput,
  condition?: ModelMessageConditionInput | null,
};

export type UpdateMessageMutation = {
  updateMessage:  {
    __typename: "Message",
    id: string,
    inhalt: string | null,
    cognitoId: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type DeleteMessageMutationVariables = {
  input: DeleteMessageInput,
  condition?: ModelMessageConditionInput | null,
};

export type DeleteMessageMutation = {
  deleteMessage:  {
    __typename: "Message",
    id: string,
    inhalt: string | null,
    cognitoId: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type CreateMessagePublicMutationVariables = {
  input: CreateMessagePublicInput,
  condition?: ModelMessagePublicConditionInput | null,
};

export type CreateMessagePublicMutation = {
  createMessagePublic:  {
    __typename: "MessagePublic",
    id: string,
    inhalt: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type UpdateMessagePublicMutationVariables = {
  input: UpdateMessagePublicInput,
  condition?: ModelMessagePublicConditionInput | null,
};

export type UpdateMessagePublicMutation = {
  updateMessagePublic:  {
    __typename: "MessagePublic",
    id: string,
    inhalt: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type DeleteMessagePublicMutationVariables = {
  input: DeleteMessagePublicInput,
  condition?: ModelMessagePublicConditionInput | null,
};

export type DeleteMessagePublicMutation = {
  deleteMessagePublic:  {
    __typename: "MessagePublic",
    id: string,
    inhalt: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type GetMessageQueryVariables = {
  id: string,
};

export type GetMessageQuery = {
  getMessage:  {
    __typename: "Message",
    id: string,
    inhalt: string | null,
    cognitoId: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type ListMessagesQueryVariables = {
  filter?: ModelMessageFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListMessagesQuery = {
  listMessages:  {
    __typename: "ModelMessageConnection",
    items:  Array< {
      __typename: "Message",
      id: string,
      inhalt: string | null,
      cognitoId: string | null,
      absender: string | null,
      createdAt: string | null,
      updatedAt: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetMessagePublicQueryVariables = {
  id: string,
};

export type GetMessagePublicQuery = {
  getMessagePublic:  {
    __typename: "MessagePublic",
    id: string,
    inhalt: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type ListMessagePublicsQueryVariables = {
  filter?: ModelMessagePublicFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListMessagePublicsQuery = {
  listMessagePublics:  {
    __typename: "ModelMessagePublicConnection",
    items:  Array< {
      __typename: "MessagePublic",
      id: string,
      inhalt: string | null,
      absender: string | null,
      createdAt: string | null,
      updatedAt: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateMessageSubscription = {
  onCreateMessage:  {
    __typename: "Message",
    id: string,
    inhalt: string | null,
    cognitoId: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type OnUpdateMessageSubscription = {
  onUpdateMessage:  {
    __typename: "Message",
    id: string,
    inhalt: string | null,
    cognitoId: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type OnDeleteMessageSubscription = {
  onDeleteMessage:  {
    __typename: "Message",
    id: string,
    inhalt: string | null,
    cognitoId: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type OnCreateMessagePublicSubscription = {
  onCreateMessagePublic:  {
    __typename: "MessagePublic",
    id: string,
    inhalt: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type OnUpdateMessagePublicSubscription = {
  onUpdateMessagePublic:  {
    __typename: "MessagePublic",
    id: string,
    inhalt: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

export type OnDeleteMessagePublicSubscription = {
  onDeleteMessagePublic:  {
    __typename: "MessagePublic",
    id: string,
    inhalt: string | null,
    absender: string | null,
    createdAt: string | null,
    updatedAt: string | null,
  } | null,
};

import {ChangeDetectorRef, Component} from '@angular/core';
import {AuthService} from './auth.service';
import {AppsyncService} from './appsync.service';
import {createMessage, createMessagePublic} from '../graphql/mutations';
import gql from 'graphql-tag';
import {listMessagePublics, listMessages} from '../graphql/queries';
import {onCreateMessage, onCreateMessagePublic} from '../graphql/subscriptions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  chatInput: any;
  userName: any;
  cognitoId: any; // cognitoId des aktuellen Nutzers
  chatmessages = [];

  publicUserName: any;
  publicChatMessages = [];
  publicChatInput: any;
  private publicChatHasBeenCalled = false;

  constructor(public authService: AuthService,
              private appsync: AppsyncService,
              private chageDetection: ChangeDetectorRef) {

    authService.currentUser.subscribe(user => {
      this.publicChatHasBeenCalled ? console.log('wurde schon aufgerufen') : this.getPublicMessages();

      if (user) {
        this.userName = user['custom:firstName'];
        this.cognitoId = user.sub;
        this.getMessages();
      } else {
        this.userName = undefined;
        this.cognitoId = undefined;
        this.chatmessages.splice(0);
        this.chageDetection.detectChanges();
      }
    });

  }


  sendMessage() {
    if (this.chatInput) {
      this.appsync.hc().then(client => {
        client.mutate({
          mutation: gql(createMessage),
          variables: {
            input: {
              inhalt: this.chatInput,
              absender: this.userName,
              cognitoId: this.cognitoId,
            }
          }
        }).then(result => {
          console.log(result);
          this.chatInput = '';
        });
      });
    }
  }

  sendPublicMessage() {
    console.log(this.publicChatInput, this.publicUserName);
    if (this.publicChatInput && this.publicUserName) {
      this.appsync.hc().then(client => {
        client.mutate({
          mutation: gql(createMessagePublic),
          variables: {
            input: {
              inhalt: this.publicChatInput,
              absender: this.publicUserName,
            }
          }
        }).then(result => {
          console.log(result);
          this.publicChatInput = '';
        });
      });
    }
  }

  getMessages() {
    this.appsync.hc().then(client => {
      client.query({
        query: gql(listMessages),
        fetchPolicy: 'network-only',
        variables: {
          limit: undefined
        }
      }).then(result => {
        console.log('alle nachrichten:', result);
        this.chatmessages = result.data.listMessages.items;
        this.uiSachen();
      });

      client.subscribe({
        query: gql(onCreateMessage)
      }).subscribe(result => {
        console.log('jemand hat eine nachricht geschrieben', result);
        this.chatmessages.push(result.data.onCreateMessage);
        this.uiSachen();
      });
    });
  }

  getPublicMessages() {
    this.publicChatHasBeenCalled = true;
    this.appsync.hc().then(client => {
      client.query({
        query: gql(listMessagePublics),
        fetchPolicy: 'network-only',
        variables: {
          limit: undefined
        }
      }).then(result => {
        console.log('alle öffentlichen nachrichten:', result);
        this.publicChatMessages = result.data.listMessagePublics.items;
        this.uiSachen();
      });

      client.subscribe({
        query: gql(onCreateMessagePublic)
      }).subscribe(result => {
        console.log('jemand hat eine nachricht im öffentlichen chat geschrieben', result);
        this.publicChatMessages.push(result.data.onCreateMessagePublic);
        this.uiSachen();
      });
    });
  }


  private uiSachen() {
    // tslint:disable-next-line:max-line-length
    this.chatmessages.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
    setTimeout(() => {
      const chatContainer = document.getElementById('chatContainer');
      chatContainer.scrollTop = chatContainer.scrollHeight;
      this.chageDetection.detectChanges();
    }, 100);

    // tslint:disable-next-line:max-line-length
    this.publicChatMessages.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
    setTimeout(() => {
      const publicChatContainer = document.getElementById('publicChatContainer');
      publicChatContainer.scrollTop = publicChatContainer.scrollHeight;
      this.chageDetection.detectChanges();
    }, 100);

    this.chageDetection.detectChanges();
  }

}

import {Injectable} from '@angular/core';
import {AmplifyService} from 'aws-amplify-angular';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public currentUser = new Subject<any>();

  public signUpConfig = [
    {
      label: 'Vorname',
      type: 'custom:firstName',
      placeholder: 'Gib deinen Vornamen ein'
    },
    {
      label: 'Email',
      type: 'email',
      placeholder: 'Gib deine E-Mail ein'
    },
    {
      label: 'Passwort',
      type: 'password',
      placeholder: 'Gib dein Passwort ein'
    },
  ];


  constructor(private amplifyService: AmplifyService) {
    amplifyService.authStateChange$.subscribe(authState => {
      if (authState.state === 'signedIn') {
        console.log('Ich bin angemeldet', authState.user);
        this.currentUser.next(authState.user.attributes);
      } else if (authState.state === 'signedOut') {
        console.log('Ausgeloggt');
        this.currentUser.next(undefined);
      }
    });
  }
}

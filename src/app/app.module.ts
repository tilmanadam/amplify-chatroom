import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {AmplifyUIAngularModule} from '@aws-amplify/ui-angular';
import {AmplifyService} from 'aws-amplify-angular';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AmplifyUIAngularModule,
  ],
  providers: [
    AmplifyService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}

import {Injectable} from '@angular/core';
import AWSAppSyncClient, {AUTH_TYPE} from 'aws-appsync/lib';
import aws_exports from '../aws-exports';
import {Auth} from 'aws-amplify';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AppsyncService {

  _hc;
  _hcPublic;

  usePublicKey = false;

  constructor(private authService: AuthService) {
    this._hc = new AWSAppSyncClient({
      url: aws_exports.aws_appsync_graphqlEndpoint,
      region: aws_exports.aws_project_region,
      auth: {
        type: AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
        jwtToken: async () => (await Auth.currentSession()).getIdToken().getJwtToken()
      },
      disableOffline: true,
    });

    this._hcPublic = new AWSAppSyncClient({
      url: aws_exports.aws_appsync_graphqlEndpoint,
      region: aws_exports.aws_project_region,
      auth: {
        type: AUTH_TYPE.AWS_IAM,
        credentials: () => Auth.currentCredentials(),
      },
      disableOffline: true,
    });

    authService.currentUser.subscribe(user => {
      if (user === undefined) {
        this.usePublicKey = true;
      } else {
        this.usePublicKey = false;
      }
    });

  }

  hc() {
    console.log('hydrated wird angefordert', this.usePublicKey);
    if (this.usePublicKey) {
      return this._hcPublic.hydrated();
    } else {
      return this._hc.hydrated();
    }
  }

}
